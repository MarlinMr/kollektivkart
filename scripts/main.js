wss_url = "wss://api.entur.io/realtime/v1/vehicles/subscriptions"
graphql_query = `
subscription {
  vehicles {
    line {
      lineRef
      lineName
    }
    lastUpdated
    location {
      latitude
      longitude
    }
    delay
    bearing
    direction
    mode
    speed
    vehicleId
  }
}
`
var airIcon = L.icon({ iconUrl: 'styles/air.png', iconSize: [64, 64],});
var busrIcon = L.icon({ iconUrl: 'styles/bus-right.png', iconSize: [64, 64],});
var buslIcon = L.icon({ iconUrl: 'styles/bus-left.png', iconSize: [64, 64],});
var bussIcon = L.icon({ iconUrl: 'styles/bus-south.png', iconSize: [64, 64],});
var busnIcon = L.icon({ iconUrl: 'styles/bus-north.png', iconSize: [64, 64],});
var metroIcon = L.icon({ iconUrl: 'styles/metro.png', iconSize: [64, 64],});
var railIcon = L.icon({ iconUrl: 'styles/rail.png', iconSize: [64, 64],});
var ferryIcon = L.icon({ iconUrl: 'styles/ferry.png', iconSize: [64, 64],});
var coachIcon = L.icon({ iconUrl: 'styles/coach.png', iconSize: [64, 64],});
var tramIcon = L.icon({ iconUrl: 'styles/tram.png', iconSize: [64, 64],});
var m = {}

function onload(){
    var mymap = L.map('mapid', { minZoom: 0, maxZoom: 18 }).setView([59.925, 10.705], 6);
    var layer = new L.tileLayer(
        'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', 
        {attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'}
    ).addTo(mymap);
    var markers = L.markerClusterGroup();

    let subscriptionsClient = new SubscriptionsTransportWs.SubscriptionClient(wss_url);
    let query = graphql_query;
    subscriptionsClient.request({ query }).subscribe(
        data => {
            for (const vehicle in data["data"]["vehicles"]){
                if (typeof m[data["data"]["vehicles"][vehicle]["vehicleId"]] != "undefined"){
                    mymap.removeLayer(m[data["data"]["vehicles"][vehicle]["vehicleId"]]);
                }
                switch (data["data"]["vehicles"][vehicle]["mode"]){
                    case "BUS":
                        dir = data["data"]["vehicles"][vehicle]["bearing"];
                        switch (true){
                            case (dir == 0):
                                i = buslIcon;
                                break;
                            case (dir < 45):
                                i = busnIcon;
                                break;
                            case (45 < dir < 135):
                                i = busrIcon;
                                break;
                            case (135 < dir < 225):
                                i = bussIcon;
                                break;
                            case (225 < dir < 315):
                                i = buslIcon;
                                break;
                            case (315 < dir):
                                i = busnIcon;
                                break;
                        }
                        break;
                    case "AIR":
                        i = airIcon;
                        break;
                    case "RAIL":
                        i = railIcon;
                        break;
                    case "TRAM":
                        i = tramIcon;
                        break;
                    case "COACH":
                        i = coachIcon;
                        break;
                    case "FERRY":
                        i = ferryIcon;
                        break;
                    case "METRO":
                        i = metroIcon;
                        break;}

                m[data["data"]["vehicles"][vehicle]["vehicleId"]] = L.marker([data["data"]["vehicles"][vehicle]["location"]["latitude"], data["data"]["vehicles"][vehicle]["location"]["longitude"]], {icon: i});
                vehicleid = data["data"]["vehicles"][vehicle]["vehicleId"];
                speed = data["data"]["vehicles"][vehicle]["speed"];
                direction = data["data"]["vehicles"][vehicle]["direction"];
                bearing = data["data"]["vehicles"][vehicle]["bearing"];
                mode = data["data"]["vehicles"][vehicle]["mode"];
                delay = data["data"]["vehicles"][vehicle]["delay"];
                lineref = data["data"]["vehicles"][vehicle]["line"]["lineRef"];
                linename = data["data"]["vehicles"][vehicle]["line"]["lineName"];
                m[data["data"]["vehicles"][vehicle]["vehicleId"]].bindPopup(
                    "ID: " + vehicleid + 
                    "<br>LineName: " + linename + 
                    "<br>linRef: " + lineref + 
                    "<br>Delay: " + delay + 
                    "s<br>Bearing: " + bearing + 
                    "<br>Speed: " + speed + 
                    "kmh<br>Mode: " + mode ).openPopup();

                mymap.addLayer(m[data["data"]["vehicles"][vehicle]["vehicleId"]]);
            }
        }
    );
}
